# Aries JPA example

Get example from https://github.com/apache/aries-jpa/blob/master/examples/README.md and experiment.

## Goals
1. Pack it into one bundle **SUCCESS**
2. Switch it to postgres **SUCCESS**
3. Switch to CDI 
4. Switch to OpenJPA?

# Installation instructions for jpa-examples

Install at least Karaf 4.0.x

## Copy DataSource config 

```
cat https://gitlab.com/n.seliverstov/aries-jpa-example/raw/master/org.ops4j.datasource-tasklist.cfg | tac -f etc/org.ops4j.datasource-tasklist.cfg
```

## Install features

```
feature:install transaction pax-jdbc-config pax-jdbc-pool-dbcp2 http-whiteboard jpa hibernate/4.3.6.Final
bundle:install -s mvn:org.postgresql/postgresql/9.4.1212
```

# Blueprint based example

```
mvn clean install
install -s mvn:org.apache.aries.jpa.example/org.apache.aries.jpa.example.tasklist/2.3.0
```

After installing the examples you can check for the services.

```
service:list EntityManagerFactory
```

You should see a service for the persistence unit "tasklist".

```
service:list TaskService
```

You should see a service provided by either the tasklist.blueprint or tasklist.ds bundle depending on the example you installed.

```
http://localhost:8181/tasklist
```

If you open the above url in a webbrowser you should see a list with one task.
Now add a task:

```
http://localhost:8181/tasklist?add&taskId=4&title=Buy more coffee
```

and check it is added to the list

```
http://localhost:8181/tasklist
```
